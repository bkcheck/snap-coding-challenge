import { Component } from 'react'
import { v4 as uuidv4 } from 'uuid'

import ComponentView from '../components/ComponentView'
import UserList from './UserList'

import './CRUDView.scss'

const localStorageKey = 'BK_CODING_CHALLENGE_USERS_LIST'

export default class CRUDView extends Component {
  constructor() {
    super()

    // Try to load users from localstorage
    let users = []
    const savedUsers = window.localStorage[localStorageKey]
    if (savedUsers) {
      users = JSON.parse(savedUsers)
    }

    this.state = {
      filter: '',
      users,
      userToEdit: null,
    }
  }

  getFilteredUsers = () => {
    const { filter, users } = this.state

    if (!filter) {
      return users
    }

    // The coding exercise says that the filter should just return users whose
    // surname starts with the filter. I've chosen to implement this as a case-
    // insensitive filter across both names -- more akin to how I'd implement a
    // filter in practice.
    return users.filter(u =>
      u.firstName.toLowerCase().includes(filter) || u.lastName.toLowerCase().includes(filter)
    )
  }

  getEmptyUserListMessage = () => {
    return this.state.filter
      ? 'There are no matching users. Try removing the filter to show all users.'
      : 'There are no users. Add one by clicking "Add New User" above.'
  }

  handleUserInputChange = (ev) => {
    const { name, value } = ev.target

    // Patch the user
    const userToEdit = this.state.userToEdit
    userToEdit[name] = value

    this.setState({
      userToEdit,
    })
  }

  onAddUserClick = () => {
    this.setState({
      userToEdit: {
        firstName: '',
        lastName: '',
      },
    })
  }

  onFilterChange = (ev) => {
    const { value } = ev.target

    this.setState({
      filter: value,
    })
  }

  onRemoveUserClick = (user) => {
    const userIndex = this.state.users.findIndex(u => u.id === user.id)
    const users = this.state.users

    users.splice(userIndex, 1)

    this.setState({
      users,
    })

    // Persist to localStorage
    window.localStorage[localStorageKey] = JSON.stringify(users)
  }

  onSaveUserClick = () => {
    const users = this.state.users
    const user = this.state.userToEdit

    if (user.id) {
      // Update the user
      const userIndex = users.findIndex(u => u.id === user.id)
      users[userIndex] = user
    } else {
      // Create the user
      const newUser = Object.assign({ id: uuidv4() }, user)
      users.push(newUser)
    }

    this.setState({
      users,
      userToEdit: null,
    })

    // Persist to localStorage
    window.localStorage[localStorageKey] = JSON.stringify(users)
  }

  onUpdateUserClick = (user) => {
    // Assign the user info to a new object so the form doesn't
    // update the underlying user before saving
    this.setState({
      userToEdit: Object.assign({}, user),
    })
  }

  render = () => {
    return (
      <div
        className="overlay-container"
      >
        <ComponentView
          header="CRUD"
          mainClass="crud-view"
        >
          <div
            className="crud-view__list-controls"
          >
            <a
              className="action"
              onClick={this.onAddUserClick}
            >
              <i className="fas fa-user-plus" />
              Add New User
            </a>
            <div>
              <label>
                Filter:
              </label>
              <input
                value={this.filter}
                onChange={this.onFilterChange}
              />
            </div>
          </div>
          <div
            className="crud-view__list"
          >
            <UserList
              onRemoveUserClick={this.onRemoveUserClick}
              onUpdateUserClick={this.onUpdateUserClick}
              users={this.getFilteredUsers()}
              emptyListMesage={this.getEmptyUserListMessage()}
            />
          </div>
        </ComponentView>
        {this.state.userToEdit &&
          <div
            className="overlay crud-view__add-user"
          >
            <h1>Add New User</h1>

            <form>
              <div
                className="input-group"
              >
                <label>First Name</label>
                <input
                  name="firstName"
                  value={this.state.userToEdit.firstName}
                  onChange={this.handleUserInputChange}
                />
              </div>
              <div
                className="input-group"
              >
                <label>Last Name</label>
                <input
                  name="lastName"
                  value={this.state.userToEdit.lastName}
                  onChange={this.handleUserInputChange}
                />
              </div>

              <div
                className="form-actions"
              >
                <button
                  type="button"
                  onClick={ this.onSaveUserClick }
                >
                  Save
                </button>
                <button
                  type="button"
                  className="cancel"
                  onClick={ () => this.setState({ userToEdit: null })}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        }
      </div>
    )
  }
}
