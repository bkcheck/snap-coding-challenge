import { Component } from 'react'

import './UserList.scss'

class RemoveButton extends Component {
  onRemoveUserClick = (ev) => {
    ev.stopPropagation()
    this.props.onRemoveUserClick(this.props.user)
  }

  render() {
    return (
      <button
        title="Remove user"
        onClick={this.onRemoveUserClick}
      >
        <i className="fas fa-ban" />
      </button>
    )
  }
}

export default class UserList extends Component {
  render = () => {
    if (!this.props.users?.length) {
      return <span>{this.props.emptyListMesage}</span>
    }

    return (
      <ul
        className="user-list"
      >
        {this.props.users.map(user =>
          <li
            key={user.id}
            onClick={() => this.props.onUpdateUserClick(user)}
          >
            <div
              className="user-list__name"
            >
              { user.lastName }, { user.firstName }
            </div>
            <div
              className="user-list__remove-button"
            >
              <RemoveButton
                user={user}
                onRemoveUserClick={this.props.onRemoveUserClick}
              />
            </div>
          </li>
        )}
      </ul>
    )
  }
}
