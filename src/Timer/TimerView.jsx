import { Component } from 'react'
import ComponentView from '../components/ComponentView'
import './TimerView.scss'

const timerIntervalMS = 100

export default class TimerView extends Component {
  constructor() {
    super()

    this.state = {
      durationSecs: 10,
      elapsedTimeSecs: 0,

      // We'll use this to cancel the interval timer when we hit the duration
      intervalToken: null,
    }
  }

  componentDidMount() {
    this.startTimer()
  }

  componentWillUnmount() {
    const { intervalToken } = this.state

    if (intervalToken) {
      clearInterval(intervalToken)
    }
  }

  getGaugeStyle = () => {
    const { durationSecs, elapsedTimeSecs} = this.state

    const percentage = Math.floor(100 * (elapsedTimeSecs / durationSecs))

    // Limit percentage to 100, otherwise user adjustments to the duration could
    // send this above 100 (and mess up the display).
    const limited = Math.min(percentage, 100)

    return {
      width: `${limited}%`
    }
  }

  onChangeDuration = (ev) => {
    const { durationSecs, elapsedTimeSecs } = this.state
    const { value } = ev.target

    // Since this is coming from a slider I don't think this can be
    // null or a non-numeric string
    const newDurationSecs = parseInt(value)

    // If duration has increased and the timer is currently stopped, restart the
    // timer now
    if (elapsedTimeSecs >= durationSecs && newDurationSecs > durationSecs) {
      this.startTimer()
    }

    this.setState({
      durationSecs: newDurationSecs,
    })
  }

  onTimerTick = () => {
    let { durationSecs, elapsedTimeSecs, intervalToken } = this.state

    elapsedTimeSecs += (timerIntervalMS / 1000)

    if (elapsedTimeSecs >= durationSecs) {
      clearInterval(intervalToken)
    }

    this.setState({ elapsedTimeSecs })
  }

  onRestartClick = () => {
    const { intervalToken } = this.state

    if (intervalToken) {
      clearInterval(intervalToken)
    }

    this.setState({
      elapsedTimeSecs: 0,
    })

    this.startTimer()
  }

  startTimer = () => {
    // The UI in the coding challenge document doesn't contain a 'start'
    // button so we'll just start the timer immediately.
    const intervalToken = setInterval(this.onTimerTick, timerIntervalMS)

    this.setState({ intervalToken })
  }

  render() {
    return (
      <ComponentView
        header="Timer"
        mainClass="timer-view"
      >
        <div
          className="timer-view__row"
        >
          Elapsed Time:
          <div
            className="timer-view__gauge__border"
          >
            <div
              className="timer-view__gauge__inner"
              style={this.getGaugeStyle()}
            >
            </div>
          </div>
        </div>
        <div
          className="timer-view__row"
        >
          <div
            className="timer-view__elapsed"
          >
            { Math.floor(this.state.elapsedTimeSecs * 10) / 10 }s
          </div>
        </div>
        <div
          className="timer-view__row"
        >
          Duration:
          <input
            type="range"
            value={this.state.durationSecs}
            min="1"
            max="60"
            onChange={this.onChangeDuration}
          />
        </div>
        <div
          className="timer-view__row"
        >
          <button
            onClick={this.onRestartClick}  
          >
            Reset
          </button>
        </div>
      </ComponentView>
    )
  }
}
