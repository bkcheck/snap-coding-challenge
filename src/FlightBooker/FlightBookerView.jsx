import { Component } from 'react'
import ComponentView from '../components/ComponentView'
import './FlightBookerView.scss'

function dateStringIsValid(str) {
  if (!str || typeof str !== 'string') {
    return false
  }

  // I'm going to say for the purposes of this exercise that if the string has
  // all three dash-separated date parts and that JS can parse it as a valid
  // date, it is in an acceptable format. Real-world I'd probably bring in luxon
  // or moment to help here.
  if (str.split('-').filter(x => x).length !== 3) {
    return false
  }

  var d = new Date(Date.parse(str))
  return d.toString() !== 'Invalid Date'
}

export default class FlightBookerView extends Component {
  constructor() {
    super()

    this.state = {
      returnDate: '',
      returnDateHasError: false,
      departureDate: '',
      departureDateHasError: false,
      mode: 'oneWay',
      successMessage: '',
    }
  }

  getReturnInputGroupClass = () => {
    const { returnDateHasError, mode } = this.state
    const classes = ['input-group']

    if (mode === 'oneWay') {
      classes.push('disabled')
    }

    if (returnDateHasError) {
      classes.push('error')
    }

    return classes.join(' ')
  }

  getDepartureInputGroupClass = () => {
    const { departureDateHasError } = this.state
    const classes = ['input-group']

    if (departureDateHasError) {
      classes.push('error')
    }

    return classes.join(' ')
  }

  handleDateChange = (ev) => {
    const { name, value } = ev.target

    this.setState({
      [name]: value,
    })
  }

  onBookClick = () => {
    const { returnDate, departureDate, mode } = this.state

    const departureDateHasError = !dateStringIsValid(departureDate)
    const returnDateHasError = mode === 'roundTrip' && !dateStringIsValid(returnDate)

    if (departureDateHasError || returnDateHasError) {
      this.setState({
        returnDateHasError,
        departureDateHasError,
      })
      return
    }

    const successMessage =
      `You have booked a ${mode === 'oneWay' ? ' one-way ' : ' return '} flight on ${departureDate}.`

    this.setState({
      returnDateHasError: false,
      departureDateHasError: false,
      successMessage,
    })

    setTimeout(() => this.setState({ successMessage: '' }), 5000)
  }

  onModeChange = (ev) => {
    const { value } = ev.target

    this.setState({
      mode: value,
    })
  }

  render() {
    return (
      <ComponentView
        header="Flight Booker"
        mainClass="flight-booker-view"
      >
        <div
          className="input-group"
        >
          <label>Trip Type</label>
          <select
            value={this.state.mode}
            onChange={this.onModeChange}
          >
            <option value="oneWay">one-way flight</option>
            <option value="roundTrip">return flight</option>
          </select>
        </div>
        <div
          className={this.getDepartureInputGroupClass()}
        >
          <label>Depart</label>
          <input
            name="departureDate"
            value={this.state.departureDate}
            onChange={this.handleDateChange}
            placeholder="5-28-2021"
          />
        </div>
        <div
          className={this.getReturnInputGroupClass()}
        >
          <label>Return</label>
          <input
            name="returnDate"
            value={this.state.returnDate}
            onChange={this.handleDateChange}
            disabled={this.state.mode === 'oneWay'}
            placeholder="6-3-2021"
          />
        </div>
        <button
          onClick={this.onBookClick}
        >
          Book
        </button>
        {this.state.successMessage &&
          <span
            className="flight-booker-view__success-message"
          >
            {this.state.successMessage}
          </span>
        }
      </ComponentView>
    )
  }
}
