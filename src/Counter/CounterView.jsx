import { Component } from 'react'
import ComponentView from '../components/ComponentView'
import './CounterView.scss'

export default class CounterView extends Component {
  constructor() {
    super()

    this.state = {
      count: 0
    }
  }

  increment = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  render() {
    return (
      <ComponentView
        header="Counter"
        mainClass="counter-view"
      >
        <div>
          <label>Value:</label>
          <span
            className="value-bubble"
          >
            {this.state.count}
          </span>
        </div>
        <button
          onClick={this.increment}
        >
          Count
        </button>
      </ComponentView>
    )
  }
}