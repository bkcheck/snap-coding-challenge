import { Component } from 'react'
import './TemperatureConverterView.scss'

function roundToHundredths(val) {
  return Math.round(val * 100) / 100
}

export default class TemperatureConverterView extends Component {
  constructor() {
    super()

    this.state = {
      celsius: '',
      fahrenheit: '',
    }
  }

  clearAll = () => {
    this.setState({
      celsius: '',
      fahrenheit: '',
    })
  }

  onCelsiusChange = (ev) => {
    const val = ev.target.value

    // If the textbox is empty, clear the other as well
    if (val === '') {
      this.clearAll()
    }

    const celsius = parseFloat(val)

    if (Number.isNaN(celsius)) {
      return
    }

    this.setState({
      celsius,
      fahrenheit: roundToHundredths(celsius * (9 / 5) + 32)
    })
  }

  onFahrenheitChange = (ev) => {
    const val = ev.target.value

    // If the textbox is empty, clear the other as well
    if (val === '') {
      this.clearAll()
    }
    const fahrenheit = parseFloat(ev.target.value)

    if (Number.isNaN(fahrenheit)) {
      return
    }

    this.setState({
      celsius: roundToHundredths((fahrenheit - 32) * (5/9)),
      fahrenheit,
    })
  }

  render() {
    return (
      <div
        className="component-view"
      >
        <header>
          <h1>Temperature Converter</h1>
        </header>
        <div className="component-view__main temperature-converter-view">
          <div
            className="input-group"
          >
            <label>Celsius</label>
            <input
              value={this.state.celsius}
              onChange={this.onCelsiusChange}
              maxLength="8"
            />
          </div>
          <i className="fas fa-arrows-alt-h"></i>
          <div
            className="input-group"
          >
            <label>Fahrenheit</label>
            <input
              value={this.state.fahrenheit}
              onChange={this.onFahrenheitChange}
              maxLength="8"
            />
          </div>
        </div>
      </div>
    )
  }
}