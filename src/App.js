import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'

import Nav from './components/Nav'
import CounterView from './Counter/CounterView'
import TemperatureConverterView from './TemperatureConverter/TemperatureConverterView'
import FlightBookerView from './FlightBooker/FlightBookerView'
import TimerView from './Timer/TimerView'
import CRUDView from './CRUD/CRUDView'

function App() {
  return (
    <Router>
      <div className="app">
        <Nav />

        <Switch>
          <Route path="/counter">
            <CounterView />
          </Route>
          <Route path="/temperature-converter">
            <TemperatureConverterView />
          </Route>
          <Route path="/flight-booker">
            <FlightBookerView />
          </Route>
          <Route path="/timer">
            <TimerView />
          </Route>
          <Route path="/crud">
            <CRUDView />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
