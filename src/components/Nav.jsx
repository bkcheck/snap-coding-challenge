import React from 'react'
import { Link, withRouter } from 'react-router-dom'

import './Nav.scss'

class NavBar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentPath: props.history.location.pathname
    }
    
  }

  componentDidMount = () => {
    this.props.history.listen(({ pathname }) => this.setState({ currentPath: pathname }))
  }

  getNavLinkClass = (linkPath) => {
    if (this.state.currentPath === linkPath) {
      return 'selected'
    }
  }

  render() {
    return (
      <nav>
        <ul>
          <li
            className={ this.getNavLinkClass('/counter') }
          >
            <Link to="/counter">Counter</Link>
          </li>
          <li
            className={this.getNavLinkClass('/temperature-converter')}
          >
            <Link to="/temperature-converter">Temperature Converter</Link>
          </li>
          <li
            className={this.getNavLinkClass('/flight-booker')}
          >
            <Link to="/flight-booker">Flight Booker</Link>
          </li>
          <li
            className={this.getNavLinkClass('/timer')}
          >
            <Link to="/timer">Timer</Link>
          </li>
          <li
            className={this.getNavLinkClass('/crud')}
          >
            <Link to="/crud">CRUD</Link>
          </li>
        </ul>
      </nav>
    )
  }
}

export default withRouter(NavBar)
