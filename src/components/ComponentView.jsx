import { Component } from 'react'
import './ComponentView.scss'

export default class ComponentView extends Component {
  render() {
    return (
      <div
        className="component-view"
      >
        <header>
          <h1>{ this.props.header }</h1>
        </header>
        <div className={`component-view__main ${this.props.mainClass || ''}`}>
          { this.props.children }
        </div>
      </div>
    )
  }
}